import React from 'react';
import './App.css';
import { Header } from "./components/header"
import Home from './components/home';

export default class App extends React.Component {

	constructor() {
		super();

		this.state = {
			homeLink: "Home"
		};
	}

	onGreet() {
		alert("Hello");
	}

	onChangeLinkName(newName) {
		this.setState({
			homeLink: newName
		});
	}

	render() {
		return (
			<div className="container" >
				<div className="row">
					<div className="col-xs-10 col-xs-offset-1">
						{console.log("Logging from App:" + this.state.homeLink)};

						<Header homeLink={this.state.homeLink} />
					</div>
					<div className="row">
						<div className="col-xs-10 col-xs-offset-1">
							<Home
								name={"Fernando"}
								initialAge={32}
								greet={this.onGreet}
								changeLink={this.onChangeLinkName.bind(this)}
							/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
