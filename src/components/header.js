import React from "react"
import PropTypes from 'prop-types'
import '../css/bootstrap.min.css'

export const Header = (props) => {
    return (
        <nav className="navbar navbar-default">
            <div className="container">
                <div className="navbar-header">
                    <ul className="nav navbar-nav">
                        <li><a href="/">{props.homeLink}</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

Header.propTypes = {
    homeLink: PropTypes.string,
}